import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FaceSnap } from '../models/face-snap.model';
import { FaceSnapService } from '../services/face-snaps.servevices';

@Component({
  selector: 'app-single-face-snap',
  templateUrl: './single-face-snap.component.html',
  styleUrls: ['./single-face-snap.component.scss']
})
export class SingleFaceSnapComponent implements OnInit {
  faceSnap!: FaceSnap;
  titleButton!:string;

  constructor(private faceSnapService: FaceSnapService,
                      private route: ActivatedRoute){}

  ngOnInit(): void {
    this.titleButton = "Oh snap!";
    const faceSnapId = +this.route.snapshot.params['id'];
    this.faceSnap = this.faceSnapService.getFaceSnapbyId(faceSnapId);
  }

  onSnap(){
    if (this.titleButton === "Oh snap!"){
      this.faceSnapService.snapFaceSnapById(this.faceSnap.id, 'snap');
      this.titleButton = "Ooops no snap!";
    } else{
      this.faceSnapService.snapFaceSnapById(this.faceSnap.id, 'unsnap');
    this.titleButton = "Oh snap!";
    }
  }
  test(){
    let i=1;
  }

}
