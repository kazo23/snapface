import { Injectable } from "@angular/core";
import { FaceSnap } from "../models/face-snap.model";

@Injectable({
  providedIn: 'root'
})

export class FaceSnapService {
  faceSnaps: FaceSnap[] = [
    {
      id:1,
      title: "Luffy",
      description: 'Tout est partit de là, une aventure extraordinnaire !',
      urlImage: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3DAXYCfy0lts-jMxe_PCMp0Z2IH1PQ2g3j73r6bFgmH5NQQB4NeEkFzxfE9jcda2NEH0&usqp=CAU",
      createDate: new Date(),
      snaps: 206,
      location: 'Vogue Merry'

    }, {
      id:2,
      title: "Nico robin",
      description: "Un voyage dans l'histoire",
      urlImage: "https://www.mangaluxe.com/dossiers/one-piece/img/nico-robin-young.jpg",
      createDate: new Date(),
      snaps: 18
    }, {
      id:3,
      title: "Zorro",
      description: "Les sabres c est la vie",
      urlImage: "https://static.zerochan.net/Roronoa.Zoro.full.881515.jpg",
      createDate: new Date(),
      snaps: 1,
      location: 'Perdu'
    }, {
      id:4,
      title: "Luffy",
      description: 'Tout est partit de là, une aventure extraordinnaire !',
      urlImage: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3DAXYCfy0lts-jMxe_PCMp0Z2IH1PQ2g3j73r6bFgmH5NQQB4NeEkFzxfE9jcda2NEH0&usqp=CAU",
      createDate: new Date(),
      snaps: 6,
      location: 'Vogue Merry'

    }, {
      id:5,
      title: "NicoRobin",
      description: "Un voyage dans l'histoire",
      urlImage: "https://www.mangaluxe.com/dossiers/one-piece/img/nico-robin-young.jpg",
      createDate: new Date(),
      snaps: 18
    }, {
      id:6,
      title: "Zorro",
      description: "Les sabres c est la vie",
      urlImage: "https://static.zerochan.net/Roronoa.Zoro.full.881515.jpg",
      createDate: new Date(),
      snaps: 4,
      location: 'Perdu'
    }];

    getAllFaceSnaps(): FaceSnap[]{
      return this.faceSnaps
    }
    getFaceSnapbyId(faceSnapId:number):FaceSnap{
      const faceSnap = this.faceSnaps.find(faceSnap => faceSnap.id === faceSnapId);
      if(!faceSnap){
        throw new Error('FaceSnap not found');
      }else{
        return faceSnap;
      }
    }
    snapFaceSnapById(faceSnapId: number, snapType: 'snap'| 'unsnap' ): void {
      const faceSnap = this.getFaceSnapbyId(faceSnapId);
      snapType === 'snap' ? faceSnap.snaps++ : faceSnap.snaps--;
  }


}
