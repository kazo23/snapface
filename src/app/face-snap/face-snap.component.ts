import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FaceSnap } from '../models/face-snap.model';
import { FaceSnapService } from '../services/face-snaps.servevices';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent implements OnInit{
  @Input() faceSnap!: FaceSnap;
  titleButton!:string;

  constructor(private faceSnapService: FaceSnapService,
              private router: Router){}

  ngOnInit(): void {
    this.titleButton = "Oh snap!";
  }

  onSnap(): void{
    if (this.titleButton === "Oh snap!"){
      this.faceSnapService.snapFaceSnapById(this.faceSnap.id, 'snap');
      this.titleButton = "Ooops no snap!";
    } else{
      this.faceSnapService.snapFaceSnapById(this.faceSnap.id, 'unsnap');
    this.titleButton = "Oh snap!";
    }
  }
  test(){
    let i=1;
  }

  onViewFaceSnap(): void{
    this.router.navigateByUrl(`facesnaps/${this.faceSnap.id}`);
  }
}
